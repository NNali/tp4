import gestionvol.Aeroport;
import gestionvol.Compagnie;
import gestionvol.Ville;
import gestionvol.Vol;
import gestionvol.Escale;
import reservation.Client;
import reservation.Passager;
import reservation.Reservation;

public class Executable {

    public static void main(String[] args) {

        Vol vol1 = new Vol("AF100");
        Vol vol2 = new Vol("AF200");

        Ville paris = new Ville("Paris");

        Aeroport aeroport1 = new Aeroport("CDG", paris);
        Aeroport aeroport2 = new Aeroport("ORLY", paris);

        Compagnie compagnie1 = new Compagnie("Air France");

        compagnie1.ajouterVol(vol1);
        compagnie1.ajouterVol(vol2);

        for(Vol v: compagnie1.getVols()) {
            System.out.println(v.getNumero());
        }

        System.out.println(vol1.getCompagnie().getNom());
        System.out.println(vol2.getCompagnie().getNom());

        vol1.setCompagnie(null);

        for(Vol v: compagnie1.getVols()) {
            System.out.println(v.getNumero());
        }

        Escale escale1 = new Escale();
        escale1.setAeroport(aeroport1);
        escale1.setVol(vol1);

        System.out.println(aeroport1.getEscales());
        System.out.println(vol1.getEscales());

        paris.ajouterAeroport(aeroport1);
        paris.ajouterAeroport(aeroport2);

        System.out.println(paris.getAeroports());

        Ville marne = new Ville("Marne la Vallée");

        aeroport1.ajouterVille(marne);

        System.out.println(aeroport1.getVilles_desservies());

    }

}