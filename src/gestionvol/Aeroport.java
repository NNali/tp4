package gestionvol;

import java.util.List;
import java.util.ArrayList;

public class Aeroport {
    
    private String nom;
    private List<Ville> villes_desservies;
    private List<Vol> departs;
    private List<Vol> arrivees;
    private List<Escale> escales;

    public Aeroport(String nom, Ville ville){
        this.nom = nom;
        this.villes_desservies = new ArrayList<>();
        this.villes_desservies.add(ville);
        this.departs = new ArrayList<>();
        this.arrivees = new ArrayList<>();
        this.escales = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public List<Ville> getVilles_desservies() {
        return villes_desservies;
    }

    public List<Escale> getEscales() {
        return escales;
    }

    public void setEscales(List<Escale> escales) {
        for(Escale e: this.escales) {
            e.setAeroportSansDoubleNavigabilite(null);
        }

        this.escales = escales;

        if(this.escales != null) {
            for(Escale e: this.escales) {
                e.setAeroportSansDoubleNavigabilite(this);
            }
        }
    }

    public void ajouterDepart(Vol v){
        departs.add(v);
    }

    public void ajouterArrivee(Vol v){
        arrivees.add(v);
    }

    public void ajouterVille(Ville v){
        //v.ajouterAeroport(this);
        villes_desservies.add(v);
    }

    /*
    public void ajouterVilleSansDoubleNavigabilite(Ville v) {
        this.villes_desservies.add(v);
    }
     */

    public void ajouterEscale(Escale escale) {
        escale.setAeroportSansDoubleNavigabilite(this);
        this.escales.add(escale);
    }

    public void enleverEscale(Escale escale) {
        escale.setAeroportSansDoubleNavigabilite(null);
        this.escales.remove(escale);
    }

    @Override
    public String toString() {
        return this.nom;
    }
}