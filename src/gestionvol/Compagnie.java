package gestionvol;

import java.util.List;
import java.util.ArrayList;

public class Compagnie {
    
    private String nom;

    private List<Vol> vols;

    public Compagnie(String nom){
        this.nom = nom;
        this.vols = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public List<Vol> getVols() {
        return vols;
    }

    public void setVols(List<Vol> vols) {
        for(Vol v: this.vols) {
            v.setCompagnieSansDoubleNaviguabilite(null);
        }

        this.vols = vols;

        if(this.vols != null) {
            for(Vol v: this.vols) {
                v.setCompagnieSansDoubleNaviguabilite(this);
            }
        }

    }

    public void ajouterVol(Vol v){
        if(this.vols != null) {
            v.setCompagnieSansDoubleNaviguabilite(this);
            this.vols.add(v);
        }
    }

    public void enleverVol(Vol v){
        if(this.vols != null) {
            v.setCompagnieSansDoubleNaviguabilite(null);
            this.vols.remove(v);
        }
    }


    protected void setVolsSansDoubleNaviguabilite(List<Vol> vols) {
        this.vols = vols;
    }

    protected void ajouterVolSansDoubleNaviguabilite(Vol v){
        this.vols.add(v);
    }

    protected void enleverVolSansDoubleNaviguabilite(Vol v){
        this.vols.remove(v);
    }

}