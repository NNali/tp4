package gestionvol;

import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.time.Duration;

public class Escale {

    private Date depart;
    private Date arrivee;
    private Duration duree;
    private Vol vol;
    private Aeroport aeroport;

    public Escale() {

    }

    public Escale(String depart, String arrivee, Vol vol, Aeroport aeroport) {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        try {
            this.depart = format.parse(depart);
            this.arrivee = format.parse(arrivee);
        } catch (Exception e) {
            throw new RuntimeException("Mauvais format");
        }

        this.vol = vol;
        this.aeroport = aeroport;

        calculerDuree();

    }


    //GETTERS

    public Date getDepart() {
        return depart;
    }

    public Date getArrivee() {
        return arrivee;
    }

    public Duration getDuree() {
        return duree;
    }

    public Vol getVol() {
        return vol;
    }

    public Aeroport getAeroport() {
        return aeroport;
    }


    //SETTERS

    public void setDepart(Date depart) {
        this.depart = depart;
        calculerDuree();
    }

    public void setArrivee(Date arrivee) {
        this.arrivee = arrivee;
        calculerDuree();
    }

    public void setVol(Vol vol) {
        this.vol = vol;
        this.vol.ajouterEscale(this);
    }

    public void setVolSansDoubleNavigabilite(Vol vol) {
        this.vol = vol;
    }

    public void setAeroport(Aeroport aeroport) {
        this.aeroport = aeroport;
        this.aeroport.ajouterEscale(this);
    }

    public void setAeroportSansDoubleNavigabilite(Aeroport aeroport) {
        this.aeroport = aeroport;
    }

    public void calculerDuree() {
        this.duree = Duration.of(this.depart.getTime() - this.arrivee.getTime(), ChronoUnit.MILLIS);
    }

    @Override
    public String toString() {
        return "Escale du vol " + this.vol.getNumero() + " à l'aéroport de " + this.aeroport.getNom() + ", arrivée : " + this.arrivee + " / départ : " + this.depart + "\nDurée : " + this.duree;
    }

}
