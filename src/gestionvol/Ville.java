package gestionvol;

import java.util.List;
import java.util.ArrayList;

public class Ville {
    
    private String nom;
    private List<Aeroport> aeroports;

    public Ville(String nom){
        this.nom = nom;
        this.aeroports = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public List<Aeroport> getAeroports() {
        return aeroports;
    }

    public void ajouterAeroport(Aeroport aeroport) {
        //aeroport.ajouterVilleSansDoubleNavigabilite(this);
        this.aeroports.add(aeroport);
    }

    /*
    public void ajouterAeroportSansDoubleNavigabilite(Aeroport aeroport) {
        this.aeroports.add(aeroport);
    }
     */

    @Override
    public String toString() {
        return this.nom;
    }
}