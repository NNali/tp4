package gestionvol;

import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.time.Duration;

import reservation.Reservation;

public class Vol {
    
    private String numero;

    private Date dateDepart;

    private Date dateArrivee;

    private Duration duree;

    private List<Reservation> reservations;

    private Aeroport a_depart;

    private Aeroport a_arrivee;

    private Compagnie compagnie;

    private List<Escale> escales;


    //CONSTRUCTORS

    public Vol(String numero){

        this.numero = numero;
        this.escales = new ArrayList<>();

    }


    public Vol(String num, String d_depart, String d_arrivee, Aeroport a_dep, Aeroport a_arr){

        this.numero = num;

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        try {
            this.dateDepart = format.parse(d_depart);
            this.dateArrivee = format.parse(d_arrivee);
        } catch (Exception e) {
            throw new RuntimeException("Mauvais format");
        }

        calculerDuree();

        this.reservations = new ArrayList<>();
        this.a_depart = a_dep;
        this.a_arrivee = a_arr;
        this.escales = new ArrayList<>();

    }


    // GETTERS

    public String getNumero(){
        return this.numero;
    }

    public Date getDateDepart() {
        return dateDepart;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public Duration getDuree() {
        return duree;
    }

    public Compagnie getCompagnie() {
        return compagnie;
    }

    public List<Escale> getEscales() {
        return escales;
    }

    //SETTERS

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setCompagnie(Compagnie compagnie) {
        if(compagnie != null){
            this.compagnie.ajouterVolSansDoubleNaviguabilite(this);
        }
        if(this.compagnie != null){
            this.compagnie.enleverVolSansDoubleNaviguabilite(this);
        }
        this.compagnie = compagnie;
    }

    protected void setCompagnieSansDoubleNaviguabilite(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public void setDateDepart(Date dateDepart) {
        this.dateDepart = dateDepart;
        if(this.dateDepart != null && this.dateArrivee != null){
            calculerDuree();
        }
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
        if(this.dateDepart != null && this.dateArrivee != null){
            calculerDuree();
        }
    }

    public void setEscales(List<Escale> escales) {
        for(Escale e: this.escales) {
            e.setVolSansDoubleNavigabilite(null);
        }

        this.escales = escales;

        if(this.escales != null) {
            for(Escale e: this.escales) {
                e.setVolSansDoubleNavigabilite(this);
            }
        }
    }


    //METHODS

    public void calculerDuree() {
        this.duree = Duration.of(dateArrivee.getTime() - dateDepart.getTime(), ChronoUnit.MILLIS);
    }

    public void ouvrir(){

        for(Reservation r: this.reservations){
            r.setEstOuvert();
        }

    }

    public void fermer(){

        for(Reservation r: this.reservations){
            r.setEstOuvert();
        }

    }

    public void ajouterEscale(Escale escale) {
        escale.setVolSansDoubleNavigabilite(this);
        this.escales.add(escale);
    }

    public void enleverEscale(Escale escale) {
        escale.setVolSansDoubleNavigabilite(null);
        this.escales.remove(escale);
    }

    public void ajouterReservation(Reservation reservation) {
        this.reservations.add(reservation);
    }

    public void enleverReservation(Reservation reservation) {
        this.reservations.remove(reservation);
    }

    @Override
    public boolean equals(Object o) {
        try {
            return ((Vol) o).getNumero().equals(this.numero);
        } catch (Exception e) {
            return false;
        }
    }

}