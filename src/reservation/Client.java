package reservation;

import java.util.List;
import java.util.ArrayList;

public class Client {
    
    private String nom;
    private String reference;
    private String paiement;
    private String contact;
    private List<Reservation> reservations;

    public Client(String nom, String ref, String paie, String contact){
        this.nom = nom;
        reference = ref;
        paiement = paie;
        this.contact = contact;
    }

    protected void ajouterReservation(Reservation res){
        this.reservations.add(res);
        res.setClientFromClient(this);
    }

    protected void ajouterReservFromSetter(Reservation res){
        this.reservations.add(res);
    }

    public void payer(Reservation r){

        if(r.estOuvert()){
            if(!r.isPaye()){
                for(Reservation res: this.reservations) {
                    if(res.equals(r)){
                        r.setPaye(true);
                        break;
                    } else {
                        System.out.println("Réservation non possédée");
                    }
                }
            } else {
                System.out.println("Cette réservation est déjà payée");
            }
        }

    }

}