package reservation;

import java.util.List;
import java.util.ArrayList;

public class Passager {
    
    private String nom;
    private List<Reservation> reservations;

    public Passager(String nom){
        this.nom = nom;
        this.reservations = new ArrayList<>();
    }

    protected void ajouterReservation(Reservation res){
        this.reservations.add(res);
        res.setPassagerFromPassager(this);
    }

    protected void ajouterReservFromSetter(Reservation res){
        this.reservations.add(res);
    }

}