package reservation;

import gestionvol.Vol;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Reservation {
    
    private int numero;
    private Date date;
    private Vol vol;
    private Client client;
    private Passager passager;
    private boolean paye;
    private boolean ouvert;

    public Reservation(int num, String date, Vol vol, Client cli, Passager pass){
        this.numero = num;

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        try {
            this.date = format.parse(date);
        } catch (Exception e) {
            throw new RuntimeException("Mauvais format");
        }

        this.vol = vol;
        this.client = cli;
        this.passager = pass;
    } 

    public Reservation(int num, String date, Vol vol, Passager pass){
        this.numero = num;

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        try {
            this.date = format.parse(date);
        } catch (Exception e) {
            throw new RuntimeException("Mauvais format");
        }

        this.vol = vol;
        this.client = null;
        this.passager = pass;
    }

    public Date getDate() {
        return date;
    }

    public boolean isPaye() {
        return paye;
    }

    public boolean estOuvert() {
        return ouvert;
    }

    public void setPaye(boolean paye) {
        this.paye = paye;
    }

    protected void setClient(Client cli){
        this.client = cli;
        cli.ajouterReservFromSetter(this);
    }

    public void setPassager(Passager passager) {
        this.passager = passager;
        passager.ajouterReservFromSetter(this);
    }

    public void setEstOuvert() {
        if(this.date.before(this.vol.getDateDepart())) {
            this.ouvert = true;
        } else {
            this.ouvert = false;
        }
    }

    protected void setClientFromClient(Client cli){
        this.client = cli;
    }

    protected void setPassagerFromPassager(Passager passager){
        this.passager = passager;
    }

    public void confirmer(){
        if(isPaye()){
            this.vol.ajouterReservation(this);
        } else {
            System.out.println("Non payé");
        }
    }

    public void annuler(){
        this.vol.enleverReservation(this);
    }

}